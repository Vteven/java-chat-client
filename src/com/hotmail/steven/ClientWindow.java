package com.hotmail.steven;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;

import java.awt.GridBagLayout;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRootPane;

import java.awt.GridBagConstraints;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.JTabbedPane;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JMenu;
import javax.swing.JList;
import java.awt.Insets;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.GridLayout;
import java.awt.event.ActionListener;
import java.util.logging.Level;
import java.awt.event.ActionEvent;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;

public class ClientWindow {

	private JTextArea txtChatHistory;
	private JFrame frmWindow;
	private JTextField txtMessage;
	private JButton btnSend;
	private DefaultListModel<String> listModel;
	private JList lstConnected;
	/**
	 * Hold this users chat client which
	 * handles sending and recieving data
	 */
	private ChatClient chatClient;
	// Keeps track of the messages appended to the message log
	private int msgCount;
	private JMenuItem mnuConnect;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ClientWindow window = new ClientWindow();
					window.frmWindow.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ClientWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmWindow = new JFrame();
		frmWindow.setTitle("Chat Client");
		frmWindow.setBounds(100, 100, 573, 413);
		frmWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    try {
		        // Set cross-platform Java L&F (also called "Metal")
		    UIManager.setLookAndFeel(
		        UIManager.getSystemLookAndFeelClassName());
		}
	    catch (UnsupportedLookAndFeelException e) {
	        // handle exception
	     }
	     catch (ClassNotFoundException e) {
	        // handle exception
	     }
	     catch (InstantiationException e) {
	        // handle exception
	     }
	     catch (IllegalAccessException e) {
	        // handle exception
	     }
		GridBagLayout gridBagLayout = new GridBagLayout();
		gridBagLayout.columnWidths = new int[]{0, 0};
		gridBagLayout.rowHeights = new int[]{0, 0};
		gridBagLayout.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gridBagLayout.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		frmWindow.getContentPane().setLayout(gridBagLayout);
		
		JPanel pnlMain = new JPanel();
		GridBagConstraints gbc_pnlMain = new GridBagConstraints();
		gbc_pnlMain.fill = GridBagConstraints.BOTH;
		gbc_pnlMain.gridx = 0;
		gbc_pnlMain.gridy = 0;
		frmWindow.getContentPane().add(pnlMain, gbc_pnlMain);
		GridBagLayout gbl_pnlMain = new GridBagLayout();
		gbl_pnlMain.columnWidths = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_pnlMain.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_pnlMain.columnWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_pnlMain.rowWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		pnlMain.setLayout(gbl_pnlMain);
		
		JPanel panel = new JPanel();
		GridBagConstraints gbc_panel = new GridBagConstraints();
		gbc_panel.gridheight = 10;
		gbc_panel.gridwidth = 18;
		gbc_panel.insets = new Insets(0, 0, 5, 5);
		gbc_panel.fill = GridBagConstraints.BOTH;
		gbc_panel.gridx = 0;
		gbc_panel.gridy = 0;
		pnlMain.add(panel, gbc_panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{22, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0};
		gbl_panel.columnWeights = new double[]{1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		txtChatHistory = new JTextArea();
		txtChatHistory.setWrapStyleWord(true);
		txtChatHistory.setEditable(false);
		GridBagConstraints gbc_txtChatHistory = new GridBagConstraints();
		gbc_txtChatHistory.gridwidth = 18;
		gbc_txtChatHistory.insets = new Insets(0, 0, 0, 5);
		gbc_txtChatHistory.fill = GridBagConstraints.BOTH;
		gbc_txtChatHistory.gridx = 0;
		gbc_txtChatHistory.gridy = 0;
		panel.add(txtChatHistory, gbc_txtChatHistory);
		
		listModel = new DefaultListModel<String>();
		lstConnected = new JList(listModel);
		lstConnected.setBorder(new TitledBorder(null, "Connected", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_lstConnected = new GridBagConstraints();
		gbc_lstConnected.gridwidth = 2;
		gbc_lstConnected.fill = GridBagConstraints.BOTH;
		gbc_lstConnected.gridx = 18;
		gbc_lstConnected.gridy = 0;
		panel.add(lstConnected, gbc_lstConnected);
		
		txtMessage = new JTextField();
		GridBagConstraints gbc_txtMessage = new GridBagConstraints();
		gbc_txtMessage.anchor = GridBagConstraints.SOUTH;
		gbc_txtMessage.gridwidth = 17;
		gbc_txtMessage.insets = new Insets(0, 0, 0, 5);
		gbc_txtMessage.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtMessage.gridx = 0;
		gbc_txtMessage.gridy = 10;
		pnlMain.add(txtMessage, gbc_txtMessage);
		txtMessage.setColumns(10);
		
		btnSend = new JButton("Send");
		
		JRootPane rootPane = SwingUtilities.getRootPane(frmWindow);
		rootPane.setDefaultButton(btnSend);
		/**
		 * Handle sending a message from this client
		 */
		btnSend.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent evt) 
			{
				chatClient.sendMessage(txtMessage.getText());
				txtMessage.setText("");
			}
		});
		GridBagConstraints gbc_btnSend = new GridBagConstraints();
		gbc_btnSend.anchor = GridBagConstraints.SOUTHEAST;
		gbc_btnSend.gridx = 17;
		gbc_btnSend.gridy = 10;
		pnlMain.add(btnSend, gbc_btnSend);
		
		JMenuBar menuBar = new JMenuBar();
		frmWindow.setJMenuBar(menuBar);
		
		JMenu mnMain = new JMenu("Main");
		menuBar.add(mnMain);
		
		mnuConnect = new JMenuItem("Connect");
		mnuConnect.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		mnuConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) 
			{
				JLabel label = new JLabel("Please enter a server to connect:");
				JTextField jtf = new JTextField("localhost:5000");
				JOptionPane.showConfirmDialog(null, new Object[]{label, jtf}, "Server Details:", JOptionPane.OK_CANCEL_OPTION);
				
				String[] data = jtf.getText().split(":");
				int port = Integer.parseInt(data[1]);
				String ip = data[0];
				
				JLabel label2 = new JLabel("Please enter your user name:");
				JTextField jtf2 = new JTextField();
				JOptionPane.showConfirmDialog(null, new Object[]{label2, jtf2}, "User name:", JOptionPane.OK_CANCEL_OPTION);
				//
				String username = jtf2.getText();
				
				JLabel label3 = new JLabel("Please enter your password:");
				JPasswordField jpf = new JPasswordField();
				JOptionPane.showConfirmDialog(null, new Object[]{label3, jpf}, "Password:", JOptionPane.OK_CANCEL_OPTION);
				
				String password = String.copyValueOf(jpf.getPassword());
				//TODO validate the username
				
				// Create the chat client
				chatClient = new ChatClient(ip, port, username);
				chatClient.start();
				
				startUpdater();
			}
		});
		mnMain.add(mnuConnect);
		
	}
	
	// Start the updater thread which
	// does the house keeping
	private void startUpdater()
	{
		// Update the window twice a second
				new Thread() 
				{
					public void run()
					{
						while(true)
						{
							if(listModel.size() != chatClient.getConnectedClients().size())
							{
								listModel.clear();
								for(String username  : chatClient.getConnectedClients().values())
								{
									listModel.addElement(username);
								}
							}
							if(msgCount < chatClient.getMessageLog().size())
							{
								// Check if there has been any messages received
								// Clear the chat history text area
								// Populate the chat history text area
								for(int i=msgCount;i<chatClient.getMessageLog().size();i++)
								{
									txtChatHistory.append(chatClient.getMessageLog().get(i) + "\n");
								}
								msgCount = chatClient.getMessageLog().size();
							}
							try {
								Thread.sleep(200);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
					}
				}.start();
	}

}
