package com.hotmail.steven;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;


public class ChatClient {
	
	// Hold all the messages on the server
	private List<String> serverLog;
	// Thread attached to this client
	private Thread serverHandler;
	// This clients DataOutputStream
	private DataOutputStream dos;
	
	private String username;
	// Hold the connected clients
	private HashMap<Integer, String> connectedClients;
	
    public ChatClient(String ip, int port, String username)
    {
    	connectedClients = new HashMap<Integer, String>();
    	
    	// Create the server log
    	serverLog = new ArrayList<String>(); 	
    	this.username = username;
    	
        try {
        	// setup BufferedReader for console / keyboard input
            BufferedReader bis = new BufferedReader(new InputStreamReader(System.in));
            
            Socket clientSocket = new Socket("localhost",5000);
            
           
            dos = new DataOutputStream(clientSocket.getOutputStream());
            final DataInputStream dis = new DataInputStream(clientSocket.getInputStream());
            
            // use keyboard input stream to read username
            
            
            dos.write(ServerConstants.SETUP_MESSAGE);
            dos.writeUTF(username);
            
            logMessage(Level.INFO, "Simple chat client.");
            
            // setup another thread to handle network I-O from ClientHandler 
            serverHandler = new Thread()
            {
                public void run()
                {
                    while(true)
                    {
                        int message;
                        try {
                            message = dis.read();
                        
        
                            // once we have setup the connection wait for a response from the server
                            if(message == ServerConstants.SETUP_ACK || message == ServerConstants.CHAT_ACK)
                            {
                            	/**
                            	 * Request the server update the client
                            	 * at a set interval
                            	 */
                            	setupClient();
                            }
                            else if(message == ServerConstants.BROADCAST_MESSAGE)
                            {
                            	String chatMsg = dis.readUTF();
                            	logMessage(Level.INFO, chatMsg);
                            	System.out.println(chatMsg);
                            } else if(message == ServerConstants.BROADCAST_CLIENT)
                            {
                            	String payload = dis.readUTF();
                            	// The connected clients in the server
                            	String[] connectedClientsData = payload.split(",");
                           
                            	connectedClients.clear();
                            	for(String clientData : connectedClientsData)
                            	{
                            		// Separate the client data into client name and port
                            		String[] clientDataSplit = clientData.split(":");
	                            	String clientName = clientDataSplit[0];
	                            	int port = Integer.parseInt(clientDataSplit[1]);
	                            	// Add to the hashmap
	                            	connectedClients.put(port, clientName);
                            	}
                            }
                            else
                            {
                            	// throw an exception if we are sent some strange message
                                throw new UnsupportedOperationException("Unknown message type");
                            }
    
                        }
                        catch (IOException e)
                        {
                        	System.err.println(e);
                            break;
                        }
                    }
                }
            };
            
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        
    }
    
    // Start sending and receiving data
    protected void start()
    {
    	serverHandler.start();
    }
    
    /**
     * Log a message to the server, logging a message
     * as info will send the message to the server
     * @param level
     * @param message
     */
    protected void logMessage(Level level, String message)
    {
    	
    	if(level == Level.INFO)
    	{
    		serverLog.add(message);
            
    	}
    }
    
    /**
     * Sends a message from this ChatClient to the server
     * @param message
     */
    protected void sendMessage(String message)
    {
		/**
		 * Send the message to the server
		 */
        try {
			dos.write(ServerConstants.CHAT_MESSAGE);
			dos.writeUTF(message);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Get the connected clients of this chat client
     * @return
     */
    public HashMap<Integer, String> getConnectedClients()
    {
    	return connectedClients;
    }
    
    protected List<String> getMessageLog()
    {
    	return serverLog;
    }
    
    /**
     * Populate the connected list etc
     */
    private void setupClient()
    {
    	// How often this thread runs
    	long interval = 1000L;
    	
    	new Thread()
    	{
    		public void run()
    		{
    			while(true)
    			{
    				
    				updateClient();
    				// Attempt to sleep the thread
	    			try {
						sleep(interval);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
    			}
    		}
    	}.start();
    }
    
    /**
     * Send a request to the server update
     * the client
     */
    private void updateClient()
    {
    	try {
			dos.write(ServerConstants.BROADCAST_MESSAGE);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
